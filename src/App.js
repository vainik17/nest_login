import React, { useState } from 'react'
import './App.css'
import * as yup from 'yup'
import { Form, Button } from 'react-bootstrap'
import {useForm} from "react-hook-form"
import Axios from "axios"


function App() {
  // const unsubscribe = new Subject();
  // const [token, setToken] = useState("");

  const schema = yup.object().shape({
    userName: yup.string().required("username is a required field"),
    password: yup.string().required("password is a required field")
  });
  const { handleSubmit, register, errors } = useForm({
    validationSchema: schema
  });

  const onSubmit = data => {
    const jasonData = JSON.stringify(data)
    Axios.post("https://engage.infostretch.com:8082/nest-service/nest-login",  data)
      .then((response)=> {
        console.log("NEST RESPONSE",response)
      })
      .catch(
        (error) => console.log("catch error: ",error)
      )
    // setDisabled(true);
    // Axios.post('https://nest.infostretch.com/nativefw/services/login', data, {})
    //   .then(function (response) {
    //     console.log(response);
    //   })
    //   .catch(function (error) {
    //     console.log(error);
    //   });
  };

  return (
    <div className="simple_container">
      <p className="p"> Login</p>
      <Form onSubmit={handleSubmit(onSubmit)}>
        <div>
          <label className="label">Username</label>
          <input type="text" name="userName" ref={register} className="input"/>
        </div>
        <div>
          <label className="label">password</label>
          <input type="password" name="password" ref={register} className="input"/>
          <Button variant="primary" type="submit">
            Login
          </Button>
        </div>
      </Form>
    </div>
  );
}

export default App;
